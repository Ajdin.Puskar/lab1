package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    Random rd = new Random();
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

        String r = "rock";
        String p = "paper";
        String s = "scissors";

        String [] plays = {r, p, s};

        boolean quit = false;

        String computerInput;
        String userInput;
        String continueInput;

        int index; 

        while (!quit){
            System.out.println("Let's play round " + roundCounter);
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            userInput = sc.nextLine();

            index = rd.nextInt(3);
            computerInput = plays[index];

            if (userInput.equals(computerInput)) {
                System.out.println("Human chose " + userInput + " computer chose " + computerInput + ". It's a tie!");
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            
            } else if (userInput.equals(r) && computerInput.equals(p)) {
                System.out.println("Human chose " + userInput + " computer chose " + computerInput + ". Computer wins!");
                computerScore += 1;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            } else if (userInput.equals(p) && computerInput.equals(s)) {
                System.out.println("Human chose " + userInput + " computer chose " + computerInput + ". Computer wins!");
                computerScore += 1;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            } else if (userInput.equals(s) && computerInput.equals(r)) {
                System.out.println("Human chose " + userInput + " computer chose " + computerInput + ". Computer wins!");
                computerScore += 1;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                
            } else if (userInput.equals(r) && computerInput.equals(s)) {
                System.out.println("Human chose " + userInput + " computer chose " + computerInput + ". Human wins!");
                humanScore += 1;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                
            } else if (userInput.equals(p) && computerInput.equals(r)) {
                System.out.println("Human chose " + userInput + " computer chose " + computerInput + ". Human wins!");
                humanScore += 1;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                
            } else if (userInput.equals(s) && computerInput.equals(p)) {
                System.out.println("Human chose " + userInput + " computer chose " + computerInput + ". Human wins!");
                humanScore += 1;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                
            } else {
                System.out.println("I do not understand " + userInput + ". Could you try again?");
            } 

            System.out.println("Do you wish to continue playing? (y/n)?");
            continueInput = sc.nextLine();
            if (continueInput.equals("y")){
                roundCounter += 1;
            } else if (continueInput.equals("n")) {
                System.out.println("Bye bye :)");
                break;
                
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
